const axios = require('axios');
const cjs = require("crypto-js");
const fetch = require('node-fetch');

module.exports = {
    makeTransaction: async (req, res) => {
        const currentIndex = 0
        //Configuration
        const url = "https://mercury-uat.phonepe.com";
        const mid = "UATMERCHANT";
        const tid = req.body.transactionId;
        const saltKey = "8289e078-be0b-484d-ae60-052f117f8deb";
        const saltIndex = 1;

        const paths = [
            '/v4/debit'         // POST
        ];
        
        const urls =  url + paths[currentIndex];
        try {
            const decodedRequests = [{
                "merchantId": mid,
                "transactionId": tid,
                "merchantUserId" : "user101",
                "amount": req.body.amount,
                "merchantOrderId": `testOrder_${tid}`,
                "subMerchant":"",
                "message": `payment for order placed ${tid}`,
                "mobileNumber" : req.body.mobileNumber,
                "email" : req.body.email,
                "shortName" : req.body.shortName
            }
            ];
            const path = paths[currentIndex];   
            const decodedRequest = decodedRequests[currentIndex];
            let encodedRequest = decodedRequest === "" ? "" : Buffer.from(JSON.stringify(decodedRequest)).toString('base64');
            const checksum = cjs.SHA256(encodedRequest + path + saltKey).toString(cjs.enc.Hex);
            var options = {
                method: 'POST',
                url: urls,
                headers: {"Content-Type": "application/json", "X-VERIFY": checksum + "###" + saltIndex,"X-REDIRECT-URL":"localhost:3000/phonePay/callBackPayment"},
                data:{"request": encodedRequest}
              };
            
            const resd = await axios(options);
            res.status(200).send({status: true, message: resd.data});
        } catch (error) {
            const errors = error.response.data;
            res.status(500).send({status: false, message: errors});
        }
    },
    getCallackStatus: async(req, res) =>{
        res.send(req.body);
    },

    getTransactinStatus: async (req, res) => {
        const saltKey = "8289e078-be0b-484d-ae60-052f117f8deb";
        const saltIndex = 1;
        const merchantId  = "UATMERCHANT";
        const transactionId  = req.params.transId;
        const url = `https://mercury-uat.phonepe.com/v3/transaction/${merchantId}/${transactionId}/status`;
        const checksum = cjs.SHA256(`/v3/transaction/${merchantId}/${transactionId}/status` + saltKey).toString(cjs.enc.Hex);
        try {
            var config = {
                method: 'get',
                url: url,
                headers: {"Content-Type": "application/json", "X-VERIFY": checksum + "###" + saltIndex},
              };
            
            const resd = await axios(config);
            res.status(200).send({status: true, message: resd.data});
        } catch (error) {
            const errors = error.response.data;
            res.status(500).send({status: false, message: errors});
        }
    },
    
    getRefundPayment: async(req, res)=> {
        const saltKey = "8289e078-be0b-484d-ae60-052f117f8deb";
        const saltIndex = 1;
        const urls = 'https://mercury-uat.phonepe.com/v3/credit/backToSource/';
        const decodedRequest = req.body;
        let encodedRequest = decodedRequest === "" ? "" : Buffer.from(JSON.stringify(decodedRequest)).toString('base64');
        const checksum = cjs.SHA256(encodedRequest + '/v3/credit/backToSource' + saltKey).toString(cjs.enc.Hex);
        const options = {
            method: 'POST', 
            url:urls,
            headers: {'Content-type': 'application/json', "X-VERIFY": checksum + "###" + saltIndex},
            data:{"request": encodedRequest}
        };

        try {
            const result = await axios(options);
            res.status(200).send({status: true, message: result.data.body});
        } catch (error) {
            const errors = error.response.data
            res.status(500).send(errors);
        }
    }
}
