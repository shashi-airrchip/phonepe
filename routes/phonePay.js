const express = require('express');
const phonePay = require('../controllers/phonePay/phonePayTransactions');

const axios = require('axios');

const router = express.Router();

/* GET users listing. */
router.post('/make_transaction', phonePay.makeTransaction);
router.get('/check_transaction_status/:transId/status', phonePay.getTransactinStatus);
router.get('/callBackPayment', phonePay.getCallackStatus);
router.post('/refundPayment', phonePay.getRefundPayment);

module.exports = router;
